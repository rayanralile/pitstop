from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Comment, Post, Category
from .forms import PostForm, CommentForm
from django.views import generic

# Create your views here.
from django.http import HttpResponse
from .temp_data import post_data

class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'posts/detail.html'

class PostListView(generic.ListView):
    model = Post
    template_name = 'posts/index.html'

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'posts/listcat.html'

class CategoryIndListView(generic.ListView):
    model = Category
    template_name = 'posts/list.html'

class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'posts/detailcat.html'

class PostCreateView(generic.CreateView):
    model = Post
    fields = ['titulo','post_url','conteudo','category']
    template_name = 'posts/create.html'

    def get_success_url(self):
        return reverse('posts:detail', kwargs={
            'pk': self.object.pk,
        })

class PostUpdateView(generic.UpdateView):
    model = Post
    fields = ['titulo','post_url','conteudo','category']
    template_name = 'posts/update.html'

    def get_success_url(self):
        return reverse('posts:detail', kwargs={
            'pk': self.object.pk,
        })
class PostDeleteView(generic.DeleteView):
    model = Post
    template_name = 'posts/delete.html'
    def get_success_url(self):
        return reverse('posts:index')

def search_posts(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        post_list = Post.objects.filter(name__icontains=search_term)
        context = {"post_list": post_list}
    return render(request, 'posts/search.html', context)

def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'posts/comment.html', context)