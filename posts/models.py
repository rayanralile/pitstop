from django.db import models
from django.conf import settings

class Category(models.Model):
    nome = models.CharField(max_length=50)
    descricao = models.CharField(max_length=255)
    def __str__(self):
        return f'"{self.nome}"'

class Post(models.Model):
    post_url = models.URLField(max_length=200, null=True)
    titulo = models.CharField(max_length=255)
    data_publicacao = models.DateTimeField(auto_now_add = True)
    conteudo = models.CharField(max_length=5000)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.titulo} ({self.data_publicacao})'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    date_comment = models.DateTimeField(auto_now_add = True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'