from django.forms import ModelForm
from .models import Post, Comment


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'titulo',
            'post_url',
            'conteudo',
        ]
        labels = {
            'titulo': 'Título',
            'post_url': 'URL da imagem do Post',
            'conteudo': 'Conteúdo (HTML):',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Viajante:',
            'text': 'Comentário:',
        }